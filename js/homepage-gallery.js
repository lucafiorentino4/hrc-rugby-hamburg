var allSlides = document.querySelectorAll('.slide-container');
var prevButton = document.querySelector('.prev');
var nextButton = document.querySelector('.next');
var activeSlide = 0;
var slideshowDirection = 1;
var allDots = document.querySelectorAll('.dot');
var counterBox = document.querySelector('.counter-dots');
// test if all variables are selected
//console.log(allDots);

// check if the Dots have the same length like the slides, if not then add new dots until the length is the same
if (allDots.length !== allSlides.length){
  for (var i = allDots.length, num = 1; i < allSlides.length; i++, num++) {
    var newDot = document.createElement('div');
    newDot.classList.add('dot');
    newDot.setAttribute('data-dot', i);
    var addNewNumber = document.createTextNode(num.toString());
    newDot.appendChild(addNewNumber);
    counterBox.appendChild(newDot);
}};

var allDots = document.querySelectorAll('.dot');
//console.log(allDots);

for (var i = 0; i < allDots.length; i++)
  allDots[i].addEventListener('click', function(event){
    var selectedDot = parseInt(event.target.getAttribute('data-dot'));
    //console.log(selectedDot);
    showSlide(selectedDot);
    // reset the interval of the slideshow
    clearInterval(slideshowInterval);
    slideshowInterval = setInterval(function(){
    //console.log("click on dot");
    showSlide(activeSlide + slideshowDirection)}, 3000);
  });


// click-function to change the pictures by pressing the Button Prev/Next.
prevButton.addEventListener('click', function(){
  showSlide(activeSlide -1);
  // reset the interval of the slideshow
  clearInterval(slideshowInterval);
  slideshowInterval = setInterval(function(){
  //console.log("click on prevButton");
  showSlide(activeSlide + slideshowDirection)}, 3000);
});

nextButton.addEventListener('click', function(){
  showSlide(activeSlide +1);
  // reset the interval of the slideshow
  clearInterval(slideshowInterval);
  slideshowInterval = setInterval(function(){
  //console.log("click on nextButton");
  showSlide(activeSlide + slideshowDirection)}, 3000);
});

// hide all slides and dots
function hideAllSlides(){
  for (var i = 0; i < allSlides.length; i++) {
    allDots[i].classList.remove('active');
    allSlides[i].style.display= 'none';
  }
}

// show the selected slide and dot
function showSlide(x){
  hideAllSlides();

  if (x >= allSlides.length) x = 0;
  if (x < 0) x = allSlides.length-1;

  activeSlide = x;
  allDots[x].classList.add('active');
  allSlides[x].style.display= 'block';
}

//console.log("test");
var slideshowInterval = setInterval(function(){
//console.log("start timer");
showSlide(activeSlide + slideshowDirection)}, 3000);



showSlide(activeSlide);
