// Formular validation
var formular = document.querySelector('form');
var allInputs = document.querySelectorAll('input');
var errorMessageList = document.querySelector('form.formular > div.errorBox > ul.list');
var passwordInput = document.querySelector('#password');
var agb = document.querySelector('#agb')
var errorList = []; // List of error Messages
var sendedForm = document.querySelector('.send-formular')

//test if all selected variables are correct
//console.log(errorMessageList);


//  a function that removes the class "error" on keydown
allInputs.forEach(function(input){
  input.addEventListener('keydown', function(){
    input.parentNode.classList.remove('error');
  })
});


// a function that checks if an input is correct compiled or not
function checkInput(input){
  var name = input.name;
  var value = input.value;

  switch (name) {
    case 'firstname':
    if (value === ''){
      input.parentNode.classList.add('error');
      errorList.push('Das Feld ist leer, fügen Sie Ihr Vorname ein.');
      }
      break;
    case 'surname':
    if (value === ''){
      input.parentNode.classList.add('error');
      errorList.push('Das Feld ist leer, fügen Sie Ihr Nachname ein.');
      }
      break;
    case 'e-mail':
    if (value === ''){
      input.parentNode.classList.add('error');
      errorList.push('Das Feld ist leer, fügen Sie Ihre E-Mail ein.');
    } else {
      var regex = /^[a-zA-Z0-9.!#$%&'+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)$/;
      if (regex.test(value) === false){
        input.parentNode.classList.add('error');
        errorList.push('Diese E-Mail ist ungültig.');
      }
    }
      break;
    case 'phone':
    if (value === ''){
      input.parentNode.classList.add('error');
      errorList.push('Das Feld ist leer, fügen Sie Ihre Telefon Nummer ein.');
    } else {
      var regex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
      if (regex.test(value) === false){
        input.parentNode.classList.add('error');
        errorList.push('Diese Nummer ist ungültig, fügen Sie bitte nur Zahlen ein.');
      }
    }
      break;
    case 'password':
        if (value === ''){
      input.parentNode.classList.add('error');
      errorList.push('Das Feld ist leer, fügen Sie ein Passort ein.');
    } else {
      var strongRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*?[#?!@$%^&*-_])(?=.{8,})/;
      if (strongRegex.test(value) === false){
          input.parentNode.classList.add('error');
          errorList.push('Ihr Passwort muss mindestens ' + 8 + ' Zeichen Lang sein, muss mind. ' + 1 + ' Groß- und Kleinbuchstabe, ' + 1 + ' Sonderzeichen (!?$§#...) enthalten, ' + 1 + ' Zahl.');
      }
    }
      break;
    case 'pass-confirm':
    if (value === ''){
      input.parentNode.classList.add('error');
      errorList.push('Das Feld ist leer, bitte tragen Sie die Wiederholung des Passwortes ein.');
    } else {
      if (value !== passwordInput.value){
        input.parentNode.classList.add('error');
        errorList.push('Bitte überprüfen Sie, ob die Passwort-Wiederholung mit dem eingegebenen Passwort übereinstimmt.');
      }
    }
      break;

    case 'agb':
    if (agb.checked !== true){
      input.parentNode.classList.add('error');
      errorList.push('Bitte bestätigen Sie unsere AGBs.');
    }
      break;
    }
}


// a function that creates new messages and displays it in the error message box in the html
function prepareErrMessage(){

//  remove all old "li" from "ul" in the html
  //console.log('not clear list');
  errorMessageList.innerHTML = '';
  //console.log('clear list');

// create new error messages
  errorList.forEach(function(msg) {
    var newListEntry = document.createElement('li');
    var errorTriangle = newListEntry.appendChild(document.createElement('i'));
    errorTriangle.classList.add('fas');
    errorTriangle.classList.add('fa-exclamation-triangle');
    newListEntry.appendChild(document.createTextNode(msg));
    errorMessageList.appendChild(newListEntry);
  });
}


// add an event to the submit button and check the inputs
formular.addEventListener('submit', function(event){
  event.preventDefault();

  errorList = [];

// check every input
  allInputs.forEach(function(element){
    element.parentNode.classList.remove('error');
    checkInput(element);
  });

  //console.log('checkerrors');

// start to prepare the messages in the list
  prepareErrMessage();
  //console.log('checked prepare message works');

// check if there are error messages in the variable "errorList", if there are show the error message box, if not hide it
  if (errorList.length !== 0){
    errorMessageList.parentNode.style.display = 'block';
    sendedForm.style.display = "none";
  } else {
    errorMessageList.parentNode.style.display = 'none';
    console.log('submit');

// send the correct compiled Formular
    //sendFormular.submit();
    sendedForm.style.display = "block";
  }
});
