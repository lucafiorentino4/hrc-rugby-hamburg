function getMq() {
  return window.getComputedStyle(document.querySelector('.sidecontent'), ':before').getPropertyValue('content').replace(/"/g, '');
}

if(getMq() == "S"){
  //console.log("Media Query S beim Aufruf aktiv");
  console.log('You are non a Mobile device.');
} else if(getMq() == "L"){
  //console.log("Media Query L beim Aufruf aktiv");
  console.log('You are non a desktop device.');

//-----------------------------------------------------------------------------------------

  var lightboxContainer = document.querySelector('.lightbox-container');
  var heroImage = document.querySelector('#heroImage');
  var allThumbs = document.querySelectorAll('.thumb-slide-container .thumb-pic img');
  var exitButton = document.querySelector('.exit-button');
  var body = document.querySelector('body');

  allThumbs.forEach(function(element) {
    element.addEventListener('click', function(event) {
      var fileSource = event.target.src

  // find the position of the last slash
      var slashPos = fileSource.lastIndexOf("/");

  // this is the part until the last slash of the "path" file:///C:/U...script/img/gallery/
      var path = fileSource.substring(0, slashPos + 1);
      //console.log(path);

  // this is the part after the last slash until the end is the File Name "gallery-slider-1.jpg"
      var filename = fileSource.substring(slashPos + 1, fileSource.length);
      //console.log(filename);

  // this is the last part of the File name after the "-"
      var filename_end = filename.substring(filename.indexOf("-"), filename.length);
      //console.log(filename_end);

  // here I create the link and change a part of the name of the file
      var new_fileSource = path + 'hero' + filename_end;

      heroImage.src = new_fileSource;

      lightboxContainer.style.display = 'block';

      if (lightboxContainer.style.display === 'block') {
        body.classList.add('no-scroll');
      }

  // Close opened layer by esc button
      if (lightboxContainer.style.display === "block"){
        document.onkeydown = function(evt) {
          evt = evt
          if (evt.keyCode == 27) {
            lightboxContainer.style.display = 'none';
            //console.log("Close open layer");
            body.classList.remove('no-scroll');
          };
        };
      };
    });
  });

  exitButton.addEventListener('click',function(){
  //console.log('close');
    lightboxContainer.style.display = 'none';
    body.classList.remove('no-scroll');
  });

  lightboxContainer.addEventListener('click', function(event){
    if (event.target == lightboxContainer) {
      lightboxContainer.style.display = 'none';
      body.classList.remove('no-scroll');
    };
  });
}
