var burgerMenu = document.querySelector('.burger-button')
var mobileMenu = document.querySelector('.navigation-mobile')
var body = document.querySelector('body');
//console.log(burgerMenu);


// OPEN MOBILE MENU
burgerMenu.addEventListener('click', function() {
  openCloseMobileMenu();
  //console.log('click on burger menu');
});

function hideMobileMenus(){
  if (!mobileMenu.style.maxWidth === null)
  mobileMenu.style.maxWidth = null;
  hideAllMenus();
};

// this function opens and closes the burger menu by click
function openCloseMobileMenu(){
  hideMobileMenus();
  mobileMenu.style.maxWidth = (mobileMenu.style.maxWidth) ? null : (mobileMenu.scrollWidth + 'vh');
  body.classList.toggle('no-scroll');
};

//-------------------------------------------------------------------------------------

var ddMobileMenu = document.querySelectorAll('.navi-point');
var ddMenuList = document.querySelectorAll('.dropdown-list-content-m');
//console.log(ddMobileMenu);
//console.log(ddMenuList);

// OPEN/CLOSE DROPDOWN MENU IN THE MOBILE NAVI
// this function hide all menus in the mobile menu layer
function hideAllMenus(){
  for (var i=0; i < ddMenuList.length; i++){
    ddMenuList[i].style.maxHeight = null;
    ddMobileMenu[i].classList.remove('active-list-m');
  }
};

// open/close the submenus in the mobile menu layer
for (var i = 0; i < ddMobileMenu.length; i++) {
  ddMobileMenu[i].addEventListener('click', function(){
    if (!this.classList.contains('active-list-m')){
    hideAllMenus();
    }

    // add "active-list-m" class to Selected Menu
    this.classList.toggle('active-list-m');

    // open the submenus in the mobile menu layer
    var menuList = this.nextElementSibling;
    //console.log(menuList);
    menuList.style.maxHeight = (menuList.style.maxHeight) ? null : (menuList.scrollHeight + 'px');
  });
};
